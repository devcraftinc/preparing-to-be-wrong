// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package katas.Construction.GivePeopleControl ;

import katas.Construction.Airplane;
import katas.Construction.Vehicle;

public class PlaneManufacturingAgent
  {
    public int doors;
    public int engines;
    public int windows;
    public int wings;

    public Vehicle buildPlane()
    {
      Airplane plane = constructFrame();

      buildInterior(plane);
      paint(plane);

      if (new VehicleStandard(windows, doors).isMet(plane))

        return plane;

      return null;
    }

    private void paint(Airplane plane)
    {
      // pick colors
      // paint base colors
      // paint logo
    }

    private void buildInterior(Airplane plane)
    {
      // find tiny seats
      // put them too close each other
      // tweedle moustache and sneer
    }

    private Airplane constructFrame()
    {
      return new Airplane(doors, windows, wings, engines);
    }
  }

