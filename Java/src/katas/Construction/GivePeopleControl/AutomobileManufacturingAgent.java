// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



package katas.Construction.GivePeopleControl ;

import katas.Construction.Vehicle;

public class AutomobileManufacturingAgent
  {
    public static final VehicleStandard Sedan = new VehicleStandard(6, 4);
    public static final VehicleStandard SmallTruck = new VehicleStandard(6, 4);
    public static final VehicleStandard TwoSeater = new VehicleStandard(4, 4);
    public static final VehicleStandard Van = new VehicleStandard(6, 4);
    public static final VehicleStandard CommercialTruck = new VehicleStandard(4, 6);

    public Vehicle manufacture(VehicleStandard standard)
    {
      Vehicle result = null;

      if (standard == Sedan)
        result = makeSedan();
      else if (standard == SmallTruck)
        result = makeSmallTruck();
      else if (standard == TwoSeater)
        result = makeTwoSeater();
      else if (standard == Van)
        result = makeVan();
      else if (standard == CommercialTruck)
        result = makeCommercialTruck();

      if (!standard.isMet(result))
        throw new IllegalStateException();

      return result;
    }

    private Vehicle makeSedan()
    {
      // do all the steps
      return null;
    }

    private Vehicle makeSmallTruck()
    {
      // do all the steps
      return null;
    }

    private Vehicle makeTwoSeater()
    {
      // do all the steps
      return null;
    }

    private Vehicle makeVan()
    {
      // do all the steps
      return null;
    }

    private Vehicle makeCommercialTruck()
    {
      // do all the steps
      return null;
    }
  }

