// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.




package katas.Dependencies.HideDependencies;

import katas.Dependencies._3rdParty.DisplayPreferences;
import katas.Dependencies._3rdParty.Item;
import katas.Dependencies._3rdParty.Task;

import java.util.ArrayList;
import java.util.List;

public class SpecialCaseClient
  {
    private class SpecialPreferences extends DisplayPreferences
    {
      private final List<Item> curatedList;
      private final DisplayPreferences underlying;

      public SpecialPreferences(DisplayPreferences underlying, List<Item> curatedList)
      {
        this.curatedList = curatedList;
        this.underlying = underlying;
      }

      @Override
      public boolean showInTaskList(Item item)
      {
        return underlying.showInTaskList(item) && curatedList.contains(item);
      }
    }

    private final List<Item> curatedList;

    public SpecialCaseClient(List<Item> curatedList)
    {
      this.curatedList = curatedList;
    }

    public Iterable<Task> getDisplayableTasksAddressedInCuratedDatabase(Iterable<Item> items)
    {
      DisplayPreferences preferences = DisplayPreferences.getInstance();
      preferences = new SpecialPreferences(preferences, curatedList);

      // OOPS! Need to inject new preferences.
      Iterable<Task> tasks = new SubsetExtractor().getTasks(items);

      ArrayList<Task> result = new ArrayList<>();
      for (Task task : tasks)
        result.add(task);

      return result;
    }
  }

