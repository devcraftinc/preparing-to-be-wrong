// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas.Capabilities.HideImplementationDetails;

// this design is representative
// imagine how it gets worse each time you add a client...
// imagine how it gets worse if you have external clients...
// how can you get around those problems?
public class Oven {
    private boolean _IsLit;

    private boolean IsLit() {
        return _IsLit;
    }

    public Object removeFromMiddle() {
        openDoor();
        pullOutRack(2);
        Object cooked = removeClosestObjectOnRack(2);
        pushInRack(2);
        closeDoor();
        return cooked;
    }

    public void placeInMiddle(Object uncooked) {
        openDoor();
        pullOutRack(2);
        addToRack(2, uncooked);
        pushInRack(2);
        closeDoor();
    }

    public void preheatTo(int targetTemperature) {
        setGasMark(targetTemperature / 500d);
        depressIgniter();
        do {
            sleep(50);
        } while (!IsLit());

        releaseIgniter();

        sleep(10 * 60 * 1000);
    }

    public void turnOff() {
        setGasMark(0);
    }

    private void setGasMark(double d) {
        // turn the gas up or down accordingly
    }

    private void depressIgniter() {
        // hold down the igniter
    }

    private void releaseIgniter() {
        // let up from the igniter
    }

    private void openDoor() {
        // pull open the door
    }

    private void closeDoor() {
        // push closed the door
    }

    private void pullOutRack(int rackNumber) {
        // pull out the specified rack
    }

    private void pushInRack(int rackNumber) {
        // push in the specified rack
    }

    private void addToRack(int rackNumber, Object toCook) {
        // add the object to the rack
    }

    private Object removeClosestObjectOnRack(int rackNumber) {
        // take the item off the rack and return it
        return null;
    }

    private void sleep(int milliseconds) {

    }
}

