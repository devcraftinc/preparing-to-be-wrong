// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



package katas.Capabilities.HideImplementationDetails ;

  public class SteakCookingClient
  {
    private final Oven oven;

    public SteakCookingClient(Oven oven)
    {
      this.oven = oven;
    }

    public Steak reversSearMethod(Steak steak)
    {
      oven.preheatTo(225);
      oven.placeInMiddle(steak);

      sleep(15 * 60 * 1000);

      steak = (Steak) oven.removeFromMiddle();

      // OOPS! Need a way to put the oven in "sear" mode.

      oven.placeInMiddle(steak);

      sleep(60 * 1000);

      oven.turnOff();

      return (Steak) oven.removeFromMiddle();
    }

    private void sleep(int milliseconds) {
      try {
        Thread.sleep(milliseconds);
      } catch (InterruptedException ex) {
        throw new IllegalStateException();
      }
    }
  }

