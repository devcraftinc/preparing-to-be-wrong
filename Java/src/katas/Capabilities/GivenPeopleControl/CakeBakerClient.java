// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



package katas.Capabilities.GivenPeopleControl ;

  public class CakeBakerClient
  {
    // OOPS! Might want to bake a cake on the Rt340...
    private final GasOven3000 oven = new GasOven3000();

    public Cake bakeCake(Cake uncooked)
    {
      oven.setGasMark(3d / 5d);
      oven.depressIgniter();
      do
      {
        sleep(50);
      } while (!oven.IsLit());

      oven.releaseIgniter();

      sleep(10 * 60 * 1000);

      oven.openDoor();
      oven.pullOutRack(2);
      oven.addToRack(2, uncooked);
      oven.pushInRack(2);
      oven.closeDoor();

      sleep(30 * 60 * 1000);
      oven.openDoor();
      oven.pullOutRack(2);
      Cake cooked = (Cake) oven.removeClosestObjectOnRack(2);
      oven.pushInRack(2);
      oven.closeDoor();

      oven.setGasMark(0);

      return cooked;
    }

    private void sleep(int milliseconds) {
      try {
        Thread.sleep(milliseconds);
      } catch (InterruptedException ex) {
        throw new IllegalStateException();
      }
    }
  }

