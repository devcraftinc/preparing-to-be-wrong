// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package katas.Capabilities.GivenPeopleControl ;

  // this design is representative
  // imagine how it gets worse each time you add a client...
  // imagine how it gets worse if you have external clients...
  // how can you get around those problems?
  public class GasOven3000
  {
private boolean _IsLit;     public boolean IsLit() { return _IsLit; }

    public void setGasMark(double d)
    {
      // turn the gas up or down accordingly
    }

    public void depressIgniter()
    {
      // hold down the igniter
    }

    public void releaseIgniter()
    {
      // let up from the igniter
    }

    public void openDoor()
    {
      // pull open the door
    }

    public void closeDoor()
    {
      // push closed the door
    }

    public void pullOutRack(int rackNumber)
    {
      // pull out the specified rack
    }

    public void pushInRack(int rackNumber)
    {
      // push in the specified rack
    }

    public void addToRack(int rackNumber, Object toCook)
    {
      // add the object to the rack
    }

    public Object removeClosestObjectOnRack(int rackNumber)
    {
      // take the item off the rack and return it
      return null;
    }

    public void pullHighHeatKnob()
    {
      // pull out on the high-heat knob and hold it there
    }

    public void releaseHighHeatKnob()
    {
      // release the high-heat knob
    }

    public void turnHighHeatKnobTo(boolean isOn)
    {
      // turn the position of the high-heat knob to the target position
      // false: off - no convection fan, normal gas consumption
      // true: on - convection fan on, extra gas ports open
    }
  }

