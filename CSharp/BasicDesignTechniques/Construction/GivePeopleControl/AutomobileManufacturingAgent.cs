﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace BasicDesignTechniques.Construction.GivePeopleOptions
{
  public class AutomobileManufacturingAgent
  {
    public static readonly VehicleStandard Sedan = new VehicleStandard(6, 4);
    public static readonly VehicleStandard SmallTruck = new VehicleStandard(6, 4);
    public static readonly VehicleStandard TwoSeater = new VehicleStandard(4, 4);
    public static readonly VehicleStandard Van = new VehicleStandard(6, 4);
    public static readonly VehicleStandard CommercialTruck = new VehicleStandard(4, 6);

    public Vehicle Manufacture(VehicleStandard standard)
    {
      Vehicle result = null;

      if (standard == Sedan)
        result = MakeSedan();
      else if (standard == SmallTruck)
        result = MakeSmallTruck();
      else if (standard == TwoSeater)
        result = MakeTwoSeater();
      else if (standard == Van)
        result = MakeVan();
      else if (standard == CommercialTruck)
        result = MakeCommercialTruck();

      if (!standard.IsMet(result))
        throw new InvalidOperationException();

      return result;
    }

    private Vehicle MakeSedan()
    {
      // do all the steps
      return null;
    }

    private Vehicle MakeSmallTruck()
    {
      // do all the steps
      return null;
    }

    private Vehicle MakeTwoSeater()
    {
      // do all the steps
      return null;
    }

    private Vehicle MakeVan()
    {
      // do all the steps
      return null;
    }

    private Vehicle MakeCommercialTruck()
    {
      // do all the steps
      return null;
    }
  }
}