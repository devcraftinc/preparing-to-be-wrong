﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace BasicDesignTechniques.Construction.GivePeopleOptions
{
  public class AirplaneSafetyInspector
  {
    private readonly VehicleStandard standard;

    // OOPS! We wish VehicleStandard was an abstraction but it's concrete.
    // What will it cost to be wrong, here?
    public AirplaneSafetyInspector(VehicleStandard standard)
    {
      this.standard = standard;
    }

    public void Inspect(Airplane plane)
    {
      DoGenericInitialPaperwork(plane);

      if (!standard.IsMet(plane))
        Reject(plane);
      else
        Approve(plane);

      DoGenericFinalPaperwork(plane);
    }

    private void DoGenericInitialPaperwork(Airplane plane)
    {
      // blah blah blah
    }

    private void Reject(Airplane plane)
    {
      // it is hereby declared that this plane is junk
    }

    private void Approve(Airplane plane)
    {
      // even though I'm very important, I still have to let a few planes through
    }

    private void DoGenericFinalPaperwork(Airplane plane)
    {
      // blah blah blah
    }
  }
}