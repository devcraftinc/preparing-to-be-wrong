﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Threading;

namespace BasicDesignTechniques.Capabilities.HideImplementationDetails
{
  // this design is representative
  // imagine how it gets worse each time you add a client...
  // imagine how it gets worse if you have external clients...
  // how can you get around those problems?
  public class Oven
  {
    private bool IsLit { get; }

    public object RemoveFromMiddle()
    {
      OpenDoor();
      PullOutRack(2);
      var cooked = RemoveClosestObjectOnRack(2);
      PushInRack(2);
      CloseDoor();
      return cooked;
    }

    public void PlaceInMiddle(object uncooked)
    {
      OpenDoor();
      PullOutRack(2);
      AddToRack(2, uncooked);
      PushInRack(2);
      CloseDoor();
    }

    public void PreheatTo(int targetTemperature)
    {
      SetGasMark(targetTemperature / 500d);
      DepressIgniter();
      do
      {
        Thread.Sleep(50);
      } while (!IsLit);

      ReleaseIgniter();

      Thread.Sleep(10 * 60 * 1000);
    }

    public void TurnOff()
    {
      SetGasMark(0);
    }

    private void SetGasMark(double d)
    {
      // turn the gas up or down accordingly
    }

    private void DepressIgniter()
    {
      // hold down the igniter
    }

    private void ReleaseIgniter()
    {
      // let up from the igniter
    }

    private void OpenDoor()
    {
      // pull open the door
    }

    private void CloseDoor()
    {
      // push closed the door
    }

    private void PullOutRack(int rackNumber)
    {
      // pull out the specified rack
    }

    private void PushInRack(int rackNumber)
    {
      // push in the specified rack
    }

    private void AddToRack(int rackNumber, object toCook)
    {
      // add the object to the rack
    }

    private object RemoveClosestObjectOnRack(int rackNumber)
    {
      // take the item off the rack and return it
      return null;
    }
  }
}