﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace BasicDesignTechniques.Capabilities.GivenPeopleControl
{
  public class Rt340
  {
    public void RefillPellets()
    {
      // add fuel to the hopper
    }

    public void SetTargetTemp(int fahrenheit)
    {
      // punch in the number
    }

    public int GetCurrentTemp()
    {
      // read the display
      return 0;
    }

    public void Start()
    {
      // tell the grill to start
    }

    public void Shutdown()
    {
      // tell the gril to turn itself off
    }

    public void OpenLid()
    {
      // pull the lid up to expose the grill grates and any food on them
    }

    public void CloseLid()
    {
      // pull the lid down to enclose the grill grates and any food on them
    }

    public void PlaceObjectOnGrate(double x, double y, object toCook)
    {
      // place the object at the specified location
    }

    public object RemoveObjectFromGrate(double x, double y)
    {
      // remove the object from the  specified coordinates
      return null;
    }
  }
}