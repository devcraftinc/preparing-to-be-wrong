﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using BasicDesignTechniques.Dependencies._3rdParty;

namespace BasicDesignTechniques.Dependencies.HideDependencies
{
  public class SpecialCaseClient
  {
    private class SpecialPreferences : DisplayPreferences
    {
      private readonly IList<Item> curatedList;
      private readonly DisplayPreferences underlying;

      public SpecialPreferences(DisplayPreferences underlying, IList<Item> curatedList)
      {
        this.curatedList = curatedList;
        this.underlying = underlying;
      }

      public override bool ShowInTaskList(Item item)
      {
        return underlying.ShowInTaskList(item) && curatedList.Contains(item);
      }
    }

    private readonly IList<Item> curatedList;

    public SpecialCaseClient(IList<Item> curatedList)
    {
      this.curatedList = curatedList;
    }

    public IEnumerable<Task> GetDisplayableTasksAddressedInCuratedDatabase(IEnumerable<Item> items)
    {
      var preferences = DisplayPreferences.GetInstance();
      preferences = new SpecialPreferences(preferences, curatedList);

      // OOPS! Need to inject new preferences.
      var tasks = new SubsetExtractor().GetTasks(items);

      var result = new List<Task>();
      foreach (var task in tasks)
        result.Add(task);

      return result;
    }
  }
}